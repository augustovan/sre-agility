output "s3_arn" {
  description = "value"
  value       = aws_s3_bucket.this.arn
}

output "s3_bucket" {
  value       = aws_s3_bucket.this.bucket
}

output "s3_bucket_name" {
  description = "value"
  value       = aws_s3_bucket.this.bucket_domain_name
}