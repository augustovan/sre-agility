variable "name" {
  type        = string
  description = ""
  default     = "terraform-iac-agility"

}

variable "tags" {
  type        = map(string)
  description = ""
  default = {
    Name    = "Bucket-Pets"
    Project = "Pets"
    CC      = "InfraCorp"
  }
}

variable "policy" {
  type        = string
  description = ""
  default     = ""
}
variable "acl" {
  type        = string
  description = ""
  default     = "private"
}
