<h1 align="center">Welcome to Projeto Pet SRE Stack 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="https://gitlab.com/augustovan/sre-agility/-/wikis/home" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
  <a href="https://twitter.com/vitikovan" target="_blank">
    <img alt="Twitter: vitikovan" src="https://img.shields.io/twitter/follow/vitikovan.svg?style=social" />
  </a>
</p>

> Este projeto tem com objetivo criar uma stack para expor uma aplicacão em Flask(Python) diponibilizar dados sobre Cães

### 🏠 [Homepage](www.uol.com.br)

### ✨ [Doc](https://gitlab.com/augustovan/sre-agility/-/wikis/home)

## :rocket: Tecnologias Utilizadas

## Stack

| Component                 | Project / Technology                  |
| --------------------------|---------------------------------------|
| Terraform                 | [Terraform](https://www.terraform.io/docs/index.html)     |
| Container runtime         | [Docker](https://www.docker.com)      |
| Container orchestration   | [Kubernetes](https://kubernetes.io)   |
| Registry                  | [AWS ECR](https://docs.aws.amazon.com/AmazonECR/latest/userguide/what-is-ecr.html)  |
| Ci/CD                     | [Gitlab](https://docs.gitlab.com/ee/ci/) |

## Registry 
Aqui está o endereço de nosso registry no ECR da AWS. Lá voce vai encontrar os containers necessario para subir essa stack


## Para subir a stack 

Para Criar a stack K8S usando o terraform utilize o comando :

```sh
terraform init

terraform fmt

terraform plan -out="tfplan.out"  

terraform apply tfplan.out  

```

Para conectar no cluster usaro seguinte comando

```sh
aws eks update-kubeconfig --name k8s-agillity --region us-east-1

```
Para ver os nós do cluster 

```sh
kubectl get nodes -o wide

```

Para depois de criar o deploy e o services, vamos precisar criar um ingress utilizando o seguinnte comando

```sh
kubectl -n pets patch svc pets-service -p '{"spec": {"type": "LoadBalancer"}}'
```


## Run tests
Segue os prints da tela de inicio do sistema

![Pagina](./assets/evidencia_001.PNG)

![Pipeline](./assets/pipeline.PNG)


## Author

👤 **Victor Nascimento**

* Twitter: [@vitikovan](https://twitter.com/vitikovan)
* LinkedIn: [@victoranascimento](https://linkedin.com/in/victoranascimento)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
